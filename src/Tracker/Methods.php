<?php

namespace Tracker;

class Methods
{
    public const GENERATE_LINK = 'hash/generate';
    public const GET_LINK = 'hash/get';
    public const GET_STATS = 'getStats';
    public const REGISTER_REFERRAL = 'registerReferral';
    public const CALCULATE_REGISTER_BONUS = 'calculateRegistratrtionBonus';
    public const CALCULATE_ORDER_BONUS = 'calculateOrderBonus';
    public const UPDATE_CAMPAIGN_SETTINGS = 'updateCampaignSettings';
}
