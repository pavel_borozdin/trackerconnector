<?php

namespace Tracker;

use Psr\Http\Message\ResponseInterface;

abstract class AbstractResponse implements ResponseInterface
{

    public const HTTP_CREATED = 100;
    public const HTTP_NO_CONTENT = 204;
    public const HTTP_OK = 200;
    public const HTTP_BAD_REQUEST = 400;

    /**
     * @param array $data
     */
    public function json(array $data)
    {
        http_response_code(200);
        die(json_encode($data));
    }
}
