<?php

namespace Tracker;

use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;
use Tracker\Builder\HttpClientBuilder;
use Tracker\TrackingSystem\Exceptions\RequestException;
use Exception;

class Tracker implements ConnectorInterface
{
    private $providerName;
    private $basicAuthName;
    private $basicAuthPassword;
    private $url;

    protected static array $successCodes = [
        AbstractResponse::HTTP_OK,
        AbstractResponse::HTTP_OK,
        AbstractResponse::HTTP_CREATED,
        AbstractResponse::HTTP_NO_CONTENT
    ];

    public function __construct()
    {
        $this->providerName = getenv('TRACKING_SYSTEM_PROVIDER_NAME');
        $this->basicAuthName = getenv('TRACKING_SYSTEM_LOGIN');
        $this->basicAuthPassword = getenv('TRACKING_SYSTEM_PASSWORD');
        $this->url = getenv('TRACKING_SYSTEM_URL');

        $this->validateConfig();
    }

    public function generateLink(int $userId)
    {
        $response = $this->request(
            'get',
            [
                'userId' => $userId,
                'provider' => $this->providerName,
                ],
            Methods::GENERATE_LINK
        );
        return $this->parseResponse($response);
    }

    public function getLink(int $userId)
    {

        $response = $this->request(
            'get',
            [
                'userId' => $userId,
                'provider' => $this->providerName
                ],
            Methods::GET_LINK
        );
        return $this->parseResponse($response);
    }

    public function getStats(int $userId)
    {
        $response = $this->request('get', [$userId], Methods::GET_STATS);
        $result = json_decode($response->getBody()->getContents(), true);
        return $this->parseResponse($result);
    }

    public function registerReferral(int $userId, string $clickId, string $hash)
    {
        $response = $this->request('post', [$userId, $clickId, $hash], Methods::REGISTER_REFERRAL);
        $result = json_decode($response->getBody()->getContents(), true);
        return $this->parseResponse($result);
    }

    public function calculateRegistrationBonus(int $userId, int $referrerId, float $btcRate)
    {
        $response = $this->request('post', [$userId, $referrerId, $btcRate], Methods::CALCULATE_REGISTER_BONUS);
        $result = json_decode($response->getBody()->getContents(), true);
        return $this->parseResponse($result);
    }

    public function calculateOrderBonus(int $userId, int $referrerId, int $orderId, float $total, float $btcRate)
    {
        $response = $this->request('post', [$userId, $referrerId, $orderId, $total, $btcRate], Methods::CALCULATE_ORDER_BONUS);
        $result = json_decode($response->getBody()->getContents(), true);
        return $this->parseResponse($result);
    }

    public function updateCampaignSettings(string $campaignType, array $rules)
    {
        $response = $this->request('patch', [$rules], Methods::UPDATE_CAMPAIGN_SETTINGS);
        $result = json_decode($response->getBody()->getContents(), true);
        return $this->parseResponse($result);
    }

    /**
     * @param string $method
     * @param array $data
     * @param string $path
     * @param array $headers
     * @return ResponseInterface
     * @throws GuzzleException
     * @throws RequestException
     */
    public function request(string $method, array $data, string $path = '', $headers = []): ResponseInterface
    {
        $client = (new HttpClientBuilder())
            ->withMethod($method)
            ->withPath($path)
            ->withAuth([$this->basicAuthName, $this->basicAuthPassword])
            ->withHeaders($headers)
            ->withBaseUrl($this->url)
            ->withBody($data)
            ->withTimeout(1)
            ->build();
        try {
            $response = $client->execute();
            $this->validateResponse($response);
            return $response;
        } catch (Exception $e) {
            throw new RequestException($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param ResponseInterface $response
     * @throws RequestException
     */
    public function validateResponse(ResponseInterface $response): void
    {
        if (!in_array($response->getStatusCode(), static::$successCodes)) {
            throw new RequestException('Wrong response code', AbstractResponse::HTTP_BAD_REQUEST);
        }
    }

    public function parseResponse(object $response)
    {
        return json_decode($response->getBody()->getContents());
    }

    public function validateConfig()
    {
        if (empty($this->providerName) || empty($this->basicAuthName) || empty($this->basicAuthPassword) || empty($this->url)) {
            die(json_encode(['status' => 'fail', 'message' => 'Empty configuration params']));
        }
    }
}
