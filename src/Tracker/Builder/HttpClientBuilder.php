<?php

namespace Tracker\Builder;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;

class HttpClientBuilder
{
    private $baseUrl;

    private $method;

    private $path;

    private $headers;

    private $timeout = 10;

    private $body;

    private $query;

    private $auth = [];

    private $client;

    /**
     * @param string $baseUrl
     * @return HttpClientBuilder
     */
    public function withBaseUrl(string $baseUrl): HttpClientBuilder
    {
        $this->baseUrl = $baseUrl;
        return $this;
    }

    /**
     * @param array $headers
     * @return HttpClientBuilder
     */
    public function withHeaders(array $headers): HttpClientBuilder
    {
        $this->headers = $headers;
        return $this;
    }

    /**
     * @param string $method
     * @return HttpClientBuilder
     */
    public function withMethod(string $method): HttpClientBuilder
    {
        $this->method = $method;
        return $this;
    }

    /**
     * @param $body
     * @return HttpClientBuilder
     */
    public function withBody($body): HttpClientBuilder
    {
        $this->body = $body;
        return $this;
    }

    /**
     * @param mixed $query
     */
    public function withQuery($query): void
    {
        $this->query = $query;
    }


    /**
     * @param string $path
     * @return HttpClientBuilder
     */
    public function withPath(string $path): HttpClientBuilder
    {
        $this->path = $path;
        return $this;
    }

    /**
     * @param array $auth
     * @return HttpClientBuilder
     */
    public function withAuth(array $auth): HttpClientBuilder
    {
        $this->auth = $auth;
        return $this;
    }

    /**
     * @param int $timeout
     * @return HttpClientBuilder
     */
    public function withTimeout(int $timeout = 0): HttpClientBuilder
    {
        $this->timeout = $timeout;
        return $this;
    }

    /**
     * @return HttpClientBuilder
     */
    public function build(): HttpClientBuilder
    {
        $this->client = new GuzzleClient([
            'base_uri' => $this->baseUrl,
            'headers' => $this->headers,
            'auth' => $this->auth,
            'timeout' => $this->timeout,
        ]);
        return $this;
    }

    /**
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public function execute(): ResponseInterface
    {
        $params = [];
        if ($this->body) {
            if ($this->method === 'get') {
                $params = ['query' => $this->body];
            } else {
                $params = ['form_params' => $this->body];
            }
        }
        return $this->client->request($this->method, $this->baseUrl . $this->path, $params);
    }
}
