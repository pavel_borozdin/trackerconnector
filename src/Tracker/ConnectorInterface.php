<?php

namespace Tracker;

interface ConnectorInterface
{

    public function generateLink(int $userId);

    public function getLink(int $userId);

    public function getStats(int $userId);

    public function registerReferral(int $userId, string $clickId, string $hash);

    public function calculateRegistrationBonus(int $userId, int $referrerId, float $btcRate);

    public function calculateOrderBonus(int $userId, int $referrerId, int $orderId, float $total, float $btcRate);

    public function updateCampaignSettings(string $campaignType, array $rules);
}
